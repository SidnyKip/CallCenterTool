<?php
	SELECT users.id,first_name, group_leader_id, evaluation_date, call_duration, call_id, 
	(score1 + score20) as gc_count,
	(score2 + score3 + score4) as ss_count,
	(score5 + score6 + score7) as oh_count,
	(score8 + score9 + score10 + score11 + score12 + score13 ) as cs_count,
	(score14 + score15 + score16 + score17 + score18 + score19 ) as cp_count,
	(score1 + score2 + score3 + score4 + score5 + score6 + score7 + score8 + score9 + score10 + score11 + score12 + score13 + score14 + score15 + score16 + score17 + score18 + score19 + score20) as total_count,
	(score5 + score10 + score11) as compliance
	FROM evaluations_mc_ib

	$totalGC = 6; 
	$totalSS = 16;
	$totalOH = 21;
	$totalCS = 27;
	$totalCP = 30;
	$compliance = 15;

	$totalMarks = 100;
	$totalScore = $agent['total_count'];
	if($agent['compliance'] < 14.5){
		$overallQAScore = 0;
	}else{
		$overallQAScore = (round(($totalScore/$totalMarks)*100,0));
	}

	$gcScore = ($agent['gc_count']/$totalGC)*100;
	$ssScore = ($agent['ss_count']/$totalSS)*100;
	$ohScore = ($agent['oh_count']/$totalOH)*100;
	$csScore = ($agent['cs_count']/$totalCS)*100;
	$cpScore = ($agent['cp_count']/$totalCP)*100;
	$complianceScore = ($agent['compliance']/$compliance)*100;

	evaluation_id

	SELECT e.form_id, SUM(r.score) FROM evaluations e INNER JOIN evaluation_results r ON e.id = r.evaluation_id GROUP BY r.form_cat_id WHERE e.form_id = $evaluationId
	
	SELECT e.form_id, SUM(r.score) AS total_score, r.form_cat_id FROM evaluations e INNER JOIN evaluation_results r ON e.id = r.evaluation_id WHERE e.id = 12 GROUP BY r.form_cat_id


	/*$cat1 = 5;
	$cat2 = 2;
	$cat3 = 10;*/

	foreach ($results as $result) {
		SELECT e.form_id, SUM(r.score)  FROM questions q INNER JOIN form_cat fc ON q.form_cat_id = fc.id GROUP BY r.form_cat_id WHERE fc.form_id = '$result['form_id']'
	}
