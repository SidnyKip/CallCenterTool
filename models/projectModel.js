'user strict';
var sql = require('../db.js');
let obj = {};
var Evaluation = require('../models/evaluationModel.js');

//Project object constructor
var Project = function(project){
	this.name = project.name;
	this.country_id = project.country_id;
};

var Form = function(form){
	this.name = form.name;
	this.project_id = form.project_id;
};

var Question = function(question){
	this.text = question.text;
	this.marks = question.marks;
	this.description = question.description;
	this.auto_fail = question.auto_fail;
	this.form_cat_id = question.form_cat_id;
};

Project.search_project = function(name, result){
	sql.query("SELECT * FROM projects WHERE name = ?", name, (err, res) => {
		if (err) {
			result(null, err);
		}
		if (res.length > 0) {
			result(null, false);
		}else{
			result(null, true);
		}
	})
};

Project.create_project = function(newProject, result) {
	let message = '';
	let userQuery = "SELECT * FROM `projects` WHERE name = '" + newProject.name + "' AND country_id = '" + newProject.country_id + "'";
	sql.query(userQuery, (err, q_result) => {
		if (err) {
			result(null, err);
		}
		if (q_result.length > 0) {
			message = 'Project name already exists';
			result(null, message);
		} else {
			sql.query("INSERT INTO projects set ?", newProject, function (err, res) {
				if(err) {
					result(err, null);
				}
				else{
					result(null, res.insertId);
				}
			});
		}
	});
};

Project.get_projects = function(countryId, result) {
  sql.query("SELECT * FROM projects WHERE country_id = ? ", countryId, function (err, res) {             
		if(err) {
		  result(null, err);
		}
		else{
		  result(null, res);
		}
  });   
};

Project.get_user_projects = function(user_id, result) {
	sql.query("SELECT u_p.id, u_p.project_id, p.name FROM user_projects u_p INNER JOIN projects p ON p.id = u_p.project_id WHERE user_id = ?", user_id, function (err, res) {
		if(err) {
		  result(null, err);
		}
		else{
		  result(null, res);
		}
  });   
};

Project.create_project_forms = function(projectForm, result) {
	var  newForm = new Form(projectForm);
	sql.query("INSERT INTO forms set ?", newForm, function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res.insertId);
		}
	});
};

Project.get_project_forms = function(params, result) {
	var project_id = params.project_id;
	var qc_id = params.user_id;
  sql.query("SELECT p.id, p.name, p.project_id, (SELECT COUNT(id) FROM form_cat WHERE form_id = p.id) AS total FROM forms p WHERE p.project_id = ? ", project_id, function (err, forms) {             
		if(err) {
		  result(null, err);
		}
		else{
			if (qc_id == 0){
			  result(null, {'forms': forms});
			}else{
				var runQuery = 	" SELECT u.id, CONCAT( u.first_name, ' ', u.last_name ) agent_names "+
												" FROM qc_agents qca "+
												" INNER JOIN user_projects up "+
												" ON qca.agent_id = up.user_id "+
												" INNER JOIN users u ON u.id = qca.agent_id "+
												" WHERE up.project_id = " + project_id +
												" AND qca.qc_id = " + qc_id
				sql.query(runQuery, function (err, agents) {
					if(err) {
					  result(null, err);
					}
					else{
					  result(null, {'forms': forms, 'agents': agents});
					}
			  });
			}

		}
  });
};

Project.get_project_users = function( project_id, result) {
	var runQuery =" SELECT u.id, CONCAT( u.first_name, ' ', u.last_name ) user_names, u.role_id "+
								" FROM user_projects u_p "+
								" INNER JOIN users u ON u.id = u_p.user_id "+
								" WHERE u_p.project_id = " + project_id
	sql.query(runQuery, function (err, res) {
		if(err)
		  result(null, err);
	  result(null, res);
  });
};

Project.get_form = function(form_id, result) {
  var runQuery = "SELECT * FROM forms WHERE id = ? ";
  sql.query(runQuery, form_id, function (err, res) {             
		if(err) {
		  result(null, err);
		}
		else{
		  res.length > 0 ? result(null, res[0]) : result(null, 'Not found');
		}
  });   
};

Project.create_form_category = function(formCategory, result){
	var runQuery = "INSERT INTO form_cat (name, form_id) VALUES ('" + formCategory.name + "'," + formCategory.form_id + ")";
	sql.query(runQuery, function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res.insertId);
		}
	});
}

Project.get_form_categories = function(form_id, result){
  var runQuery = "SELECT c.id, c.name, (SELECT COUNT(id) FROM question WHERE form_cat_id = c.id) AS total FROM form_cat c WHERE c.form_id = '"+ form_id + "' GROUP BY c.id";
  sql.query(runQuery, function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res);
		}
	});
}

Project.get_form_category_details = function(form_cat_id, result){
	var runQuery = "SELECT c.id, c.name, f.name f_name, f.id f_id FROM form_cat c INNER JOIN forms f ON f.id = c.form_id WHERE c.id = ?";
	sql.query(runQuery, form_cat_id, function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res[0]);
		}
	});
}

Project.create_form_category_question = function(question, result){
	var  newQuestion = new Question(question);
	var runQuery = "INSERT INTO question SET ?";
	sql.query(runQuery, newQuestion, function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res.insertId);
		}
	});
};

Project.get_form_category_questions = function(form_cat_id, result){
	var runQuery = "SELECT * FROM question WHERE form_cat_id = ? ";
  sql.query(runQuery, form_cat_id , function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res);
		}
	});
}

Project.update_question = function(id, question_update, result){
	var  updateQuestion = new Question(question_update);
	var runQuery = "UPDATE question SET ? WHERE id = ?";
  sql.query(runQuery, [updateQuestion, question_update.id], function (err, res) {
	  if(err) {
		result(null, err);
	   }
	   else{   
	   result(null, res);
		}
  }); 
};

Project.remove_question = function(id, result){
	var runQuery = "DELETE FROM question WHERE id = ?";
  sql.query(runQuery, [id], function (err, res) {
		if(err) {
		  result(null, err);
		}
		else{
			result(null, res);
		}
  }); 
};

Project.create_evaluation_response = function(evaluation, result){
	console.log(evaluation);
	var gcId;
	var runQuery1 = "SELECT gc_id, project_id FROM gc_qcs WHERE qc_id = "+evaluation.qc+" AND project_id = "+evaluation.project_id;
	sql.query(runQuery1, function (err, gc_Id) {
		if(err) {
			result(null, err);
		}
		else{
			gcId = gc_Id[0]['gc_id'];
			var runQuery2 = " INSERT INTO evaluations "+
											" (agent_id,	gc_id, qc_id, form_id) "+
											" VALUES ('" + 
												evaluation.agent_id + "','" + 
												gcId + "','" + 
												evaluation.qc + "','" + 
												evaluation.form_id + 
											" ')";
			sql.query(runQuery2, function (err, eval_res) {
				if(err) {
					result(null, err);
				}
				else{
					for (var i = evaluation.data.length - 1; i >= 0; i--) {
						sql.query(" INSERT INTO evaluation_results "+
											" (score,	question_id, form_cat_id, evaluation_id) "+
											" VALUES ('" + 
													evaluation.data[i]['score'] + "','" + 
													evaluation.data[i]['q'] + "','" + 
													evaluation.data[i]['c'] + "','" + 
													eval_res.insertId + "')",
											function (err, res1) {
							if(err) {
								result(null, err);
							}
						});
					}
				  Evaluation.CalcReport( eval_res.insertId,  function(err, data) {
						for (var i = data.form_categories_score.length - 1; i >= 0; i--) {
					  	var colorCode;
						  Evaluation.Colors( data.form_categories_score[i]['cat_score'], function(err, data) { colorCode = data });
					  	data.form_categories_score[i]['color_code'] = colorCode;
						}
				  	var form_colorCode;
					  Evaluation.Colors( data.form_score, function(err, data) { form_colorCode = data });
					  data.form_colorCode = form_colorCode;
						if (err)
							result(null, err);
						result(null, data);
				  });
				}
			});
		}
	});
};

Project.update_evaluation_response = function(id, evaluation_update, result){
  sql.query(" UPDATE evaluations "+
  					" SET instance = '"+ evaluation_update.instance +
  					"', recommended_action = '" + evaluation_update.recommended_action +
  					"', comments = '"+ evaluation_update.comments +
  					"', status = '"+ evaluation_update.status +
  					"' WHERE id = " + id, function (err, res) {
	  if(err) {
			result(null, err);
	  }
	  else{   
		  result(null, res);
		}
  }); 
};

Project.get_evaluation_report = function(params, result){
  Evaluation.ReportData( params, function(err, data) {
	  if(err)
			result(null, err);
		result(null, data);
  });
}

Project.get_evaluation_response = function(res_id, result){
  Evaluation.ResponseData( res_id, function(err, data) {
	  if(err)
			result(null, err);
		result(null, data);
  });
}

module.exports= Project;
