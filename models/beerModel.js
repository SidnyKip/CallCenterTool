'user strict';
var sql = require('../db.js');

var Beer = function(beer){
  this.name = beer.name;
  this.type = beer.type;
  this.quantity = beer.quantity;
  this.userId = beer.user_id;
};

Beer.getBeers = function getBeers(result) {
	sql.query("Select * from beers", function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		}
		else{
		  console.log('beers : ', res);  

		 result(null, res);
		}
	});   
};

Beer.findById = function findById(beer_id, result) {
	sql.query("Select * from beers where id = ? ", beer_id, function (err, res) {             
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			result(null, res);
		}
	});   
};

module.exports= Beer;