'user strict';
var sql = require('../db.js');

const bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var Project = require('./projectModel.js');

var transporter = nodemailer.createTransport({
  host: 'osterhout.tchmachines.com',
  port: 465,
  secure: true,
  auth: {
    user: 'no-reply@technobrainbpo.com',
    pass: 'Welcome123'
  }
});

//User object constructor
var User = function(user){
	this.username = user.username;
	this.first_name = user.first_name;
	this.last_name = user.last_name;
	this.email = user.email;
	this.role_id = user.user_type;
	this.country_id = user.country;
	// this.project_id = user.project;
};

function generatePassword(length) {
 var result           = '';
 var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
 var charactersLength = characters.length;
 for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
 }
 return result;
}

User.createUser = function(new_user, result) {
	let message = '';
	var newUser = new User(new_user);
	let userQuery = "SELECT * FROM `users` WHERE username = '" + newUser.username + "' OR email = '" + newUser.email + "'";
	sql.query(userQuery, (err, q_result) => {
		if (err) {
			result(null, err);
		}
		if (q_result.length > 0) {
			message = 'Username or email already exists';
			result(null, message);
		} else {
			var salt = bcrypt.genSaltSync(10);
			password = generatePassword(8);
			newUser.password = bcrypt.hashSync(password, salt)
			sql.query("INSERT INTO users set ?", newUser, function (err, res) {
				if(err) {
					result(err, null);
				}
				else{
					var mailOptions = {
						FROM: '"Bpo Admin" <no-reply@technobrainbpo.com>',
						to: newUser.email,
						subject: 'Credentials',
						text: 'Hello '+newUser.username+' , this is your password: '+password 
					};
					transporter.sendMail(mailOptions, function(error, info){
					  if (error) {
							result(null, error);
					  }
					});
					var user_project = { 'user_id': res.insertId, 'project_id': new_user.project }
					sql.query("INSERT INTO user_projects set ?", user_project, function (err1, res1) {
						if(err1) {
							result(null, err1);
						}
					});

					if(newUser.role_id == '1'){
						var qc_agent = { 'agent_id': res.insertId, 'qc_id': new_user.qc }
						sql.query("INSERT INTO qc_agents set ?", qc_agent, function (err2, res2) {
							if(err2) {
								result(null, err2);
							}
						});
					}else if(newUser.role_id == '2'){
						var gc_qc = { 'qc_id': res.insertId, 'gc_id': new_user.gc }
						sql.query("INSERT INTO gc_qcs set ?", gc_qc, function (err3, res3) {
							if(err3) {
								result(null, err3);
							}
						});
					}else if(newUser.role_id == '3'){
						var tc_gc = { 'gc_id': res.insertId, 'tc_id': new_user.tc }
						sql.query("INSERT INTO tc_gcs set ?", tc_gc, function (err4, res4) {
							if(err4) {
								result(null, err4);
							}
						});
					}
					result(null, res.insertId);
				}
			});


		}
	});
};

User.user_active_toggle = function(data, result) {
	console.log(data);
  sql.query("UPDATE users SET is_active = ? WHERE id = ?", [data.active, data.user_id], function (err, res) {
	  if(err) {
			result(null, err);
		 }
		else{   
			result(null, res);
		}
	}); 
};

User.verifyPassword = function(password, hash, res) {   
/*  bcrypt.compare(password, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });*/
	bcrypt.compare(password, hash, function(err, isMatch) {
    if (err) return res(err);
    res(null, isMatch);
	});
};

User.getUserByUsername = function(username, result) {
	sql.query("SELECT * FROM users WHERE username = ? ", username, function (err, res) {
		if(err) {
			result(err, null);
		}
		else{
			result(null, res);
		}
	});   
};

User.getUserById = function userDetail(userId, result) {
	sql.query("SELECT * FROM users WHERE id = ? ", userId, function (err, res) {
		if(err) {
			result(err, null);
		}
		else{
			var tablename_down;
			var tablename_up;
			var fieldname1;
			var fieldname2;
			if(res[0]['role_id'] == 1){ //agent
				res[0]['team_up'] = 'Quality Co-ordinator';
				fieldname1 = 'agent_id';
				tablename_up = 'qc_agents';
				fieldname2 = 'qc_id';
			}else if(res[0]['role_id'] == 2){ //qc
				res[0]['team_up'] = 'Group Co-ordinator';
				res[0]['team_down'] = 'Agents';
				fieldname1 = 'qc_id';
				tablename_up = 'gc_qcs';
				fieldname2 = 'gc_id';
				tablename_down = 'qc_agents';
				fieldname3 = 'agent_id';
			}else if(res[0]['role_id'] == 3){ //gc
				res[0]['team_up'] = 'Team Co-ordinator';
				res[0]['team_down'] = 'Quality Co-ordinator';
				fieldname1 = 'gc_id';
				tablename_up = 'tc_gcs';
				fieldname2 = 'tc_id';
				tablename_down = 'gc_qcs';
				fieldname3 = 'qc_id';
			}else if(res[0]['role_id'] == 4){ //tc
				res[0]['team_down'] = 'Group Co-ordinator';
				fieldname1 = 'tc_id';
				tablename_down = 'tc_gcs';
				fieldname3 = 'gc_id';
			}

			if (res[0]['role_id'] == 1 || res[0]['role_id'] == 2 || res[0]['role_id'] == 3) { // get members_up
				sql.query(" SELECT "+
									" CONCAT( u.first_name, ' ', u.last_name ) AS names "+
									" FROM " + tablename_up + " AS t "+
									" INNER JOIN users AS u "+
									" ON u.id = t." + fieldname2 +
									" WHERE t." + fieldname1 +
									" = ? ", userId,
									function (err, team) {
					if(err) {
						result(err, null);
					}
					res[0]['members_up'] = team;
				});		
			}
			if (res[0]['role_id'] == 2 || res[0]['role_id'] == 3 || res[0]['role_id'] == 4) { // get members_down
				sql.query(" SELECT "+
									" CONCAT( u.first_name, ' ', u.last_name ) AS names "+
									" FROM " + tablename_down + " AS t "+
									" INNER JOIN users u ON u.id = t." + fieldname3 + 
									" WHERE t." + fieldname1 + 
									" = ? ", userId,
									function (err, team) {
					if(err) {
						result(err, null);
					}
					res[0]['members_down'] = team;
				});				
			}

		  Project.get_user_projects( userId, function(err, projects) {
				if (err)
					result(null, err);
				res[0]['projects'] = projects;
			  result(null, res[0]);
		  });
		}
	});   
};

User.getUsers = function(params, result) {
	var country_id = params.country_id;
	var is_active = params.is_active;
	sql.query(" SELECT "+
						" u.id, u.username, u.first_name, u.last_name, u.email, u.sap, r.role_name "+
						" FROM users u "+
						" INNER JOIN roles r ON r.id = u.role_id "+
						" WHERE country_id = "+ country_id +
						" AND is_active = "+ is_active ,
						function (err, res) {
		if(err) {
			result(null, err);
		}
		else{
			result(null, res);
		}
	});
}

User.updateById = function(id, user, result){
  sql.query("UPDATE users SET ? WHERE id = ?", [user, id], function (err, res) {
	  if(err) {
			result(null, err);
		 }
		else{   
			result(null, res);
		}
	}); 
};

User.search_email = function(email, result){
	sql.query("SELECT * FROM users WHERE email = ?", email, (err, res) => {
		if (err) {
			result(null, err);
		}
		if (res.length > 0) {
			// result(null, 'Email exists');
			result(null, false);
		}else{
			// result(null, 'Email does not exists');
			result(null, true);
		}
	})
};

User.search_username = function(username, result){
	sql.query("SELECT * FROM users WHERE username = ?", username, (err, res) => {
		if (err) {
			result(null, err);
		}
		if (res.length > 0) {
			// result(null, 'Username exists');
			result(null, false);
		}else{
			// result(null, 'Username does not exists');
			result(null, true);
		}
	})
};

User.forgot_password = function(email, result){
	sql.query("SELECT * FROM users WHERE email = ?", email, (err, result1) => {
		if (err) {
			result(null, err);
		}
		if (result1.length > 0) {
			password = generatePassword(10);
			sql.query("UPDATE users SET password = ? WHERE email = ?", [password, email], (err2, result2) => {
				if (err2) {
					result(null, err2);
				}
				var mailOptions = {
					FROM: '"Bpo Admin" <no-reply@technobrainbpo.com>',
					to: email,
					subject: 'Password reset',
					text: 'Hello '+result1[0].username+' , your password has been reset to: '+ password 
				};
				transporter.sendMail(mailOptions, function(error, info){
				  if (error) {
						result(null, error);
				  }
				});
				result(null, 'Password reset succesfully');
			});
		}
	});
};

User.change_password = function(password, result){
	let user_id = password.user_id;
	let new_password = password.new_password;
	let old_password = password.old_password;
	sql.query("SELECT * FROM users WHERE id = ? AND password = ?", [user_id, old_password], (err, result1) => {
		if (err) {
			result(null, err);
		}
		if (result1.length > 0) {
			sql.query("UPDATE users SET password = ? WHERE id = ?", [new_password, user_id], (err2, result2) => {
				if (err2) {
					result(null, err2);
				}
				result(null, 'Password reset succesfully');
			});
		}else{
			result(null, 'User does not exist or invalid old password');
		}
	});
};

User.delete_user = function(id, result){
   sql.query("DELETE FROM users WHERE id = ?", [id], function (err, res) {
		if(err) {
		  result(null, err);
		}
		else{
			result(null, res);
		}
  }); 
};

/*User.userLogin = function userLogin(username, password, result) {   
	let message = '';
	let userQuery = "SELECT * FROM `users` WHERE username = '" + username + "' AND password = '" + password + "'";
	sql.query(userQuery, (err, q_result) => {
		if (err) {
			result(null, err);
		}
		if (q_result.length > 0) {
			message = 'Username or email already exists';
			result(null, message);
		} else {
			var salt = bcrypt.genSaltSync(10);
			newUser.password = bcrypt.hashSync(password, salt)
			sql.query("INSERT INTO users set ?", newUser, function (err, res) {
				if(err) {
					result(err, null);
				}
				else{
					var mailOptions = {
						FROM: '"Bpo Admin" <no-reply@technobrainbpo.com>',
						to: newUser.email,
						subject: 'Credentials',
						text: 'Hello '+newUser.username+' , this is your password: '+newUser.password 
					};
					transporter.sendMail(mailOptions, function(error, info){
					  if (error) {
							info(null, error);
					  } else {
					  }
					});
					result(null, res.insertId);
				}
			});
		}
	});
};*/

module.exports= User;