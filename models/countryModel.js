'user strict';
var sql = require('../db.js');

//Country object constructor
var Country = function(country){
  this.name = country.name;
};

Country.createCountry = function createCountry(newCountry, result) {    
	sql.query("INSERT INTO countries set ?", newCountry, function (err, res) {
		if(err) {
		  result(err, null);
		}
		else{
		  result(null, res.insertId);
		}
  });           
};
Country.getCountryById = function getCountryById(countryId, result) {
  sql.query("Select * from countries where id = ? ", countryId, function (err, res) {             
		if(err) {
		  result(null, err);
		}
		else{
		  result(null, res);
		}
  });   
};
Country.getAllCountries = function getAllCountries(result) {
	sql.query("Select * from countries", function (err, res) {
		if(err) {
		  result(null, err);
		}
		else{
			result(null, res);
		}
  });   
};
Country.updateById = function(id, country, result){
  sql.query("UPDATE countries SET country = ? WHERE id = ?", [country.country, id], function (err, res) {
	  if(err) {
		result(null, err);
	   }
	   else{   
	   result(null, res);
		}
  }); 
};
Country.remove = function(id, result){
   sql.query("DELETE FROM countries WHERE id = ?", [id], function (err, res) {
		if(err) {
		  result(null, err);
		}
		else{
			result(null, res);
		}
  }); 
};

module.exports= Country;