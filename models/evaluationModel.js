'user strict';
var sql = require('../db.js');

//Project object constructor
var Evaluation = function(){ };

Evaluation.CalcReport = function(eval_id, result){
	var runQuery = 	" SELECT "+ 
									"	e.agent_id, e.gc_id, e.qc_id, e.form_id, SUM(r.score) AS evaluation_score, r.form_cat_id, "+
									" (SELECT ROUND(SUM(marks),2) FROM question WHERE form_cat_id = r.form_cat_id) AS possible_score, "+
									" (SELECT auto_fail FROM question WHERE id = r.question_id) AS auto_fail, "+
									// " (SELECT auto_fail FROM form_cat WHERE id = r.form_cat_id) AS auto_fail, "+
									" (SELECT CONCAT( first_name, ' ', last_name ) FROM users WHERE id = e.agent_id) AS agent_names, "+
									" (SELECT name FROM form_cat WHERE id = r.form_cat_id) AS form_cat_name "+
									" FROM evaluations e "+
									" INNER JOIN evaluation_results r ON e.id = r.evaluation_id "+
									" WHERE e.id = '"+ eval_id +
									"' GROUP BY r.form_cat_id";
	sql.query(runQuery, function (err, res2) {
		if(err) {
		  result(null, err);
		}
		else{
			var form_evaluation_score = 0;
			var form_possible_score = 0;
			for (var i = res2.length - 1; i >= 0; i--) {
				res2[i]['cat_score'] = Math.round((res2[i]['evaluation_score']/res2[i]['possible_score'])*100);
				form_evaluation_score = form_evaluation_score + res2[i]['evaluation_score'];
				form_possible_score = form_possible_score + res2[i]['possible_score'];
			}
			var has_auto_fail = false;
			for (var j = res2.length - 1; j >= 0; j--) {
		    if (res2[j].auto_fail == 1) {
	        has_auto_fail = true;
	        break;
		    }
			}
			has_auto_fail==true && form_evaluation_score!=form_possible_score ? form_evaluation_score=0 : form_evaluation_score=Math.round((form_evaluation_score/form_possible_score)*100);
			obj = {
							"form_score": form_evaluation_score,
							"eval_res_id": eval_id,
							"gc_id": res2[0]['gc_id'],
							"qc_id": res2[0]['qc_id'],
							"agent_id": res2[0]['agent_id'],
							"agent_names": res2[0]['agent_names'],
							"form_categories_score": res2
						}
		  result(null, obj);
		}
	});
}

Evaluation.Colors = function( score, result){
	if (score >= 90) {
	  result(null, '#008000');
	}else if (score < 90 && score >= 80) {
	  result(null, '#FFBF00');
	}else{
	  result(null, '#FF0000');
	}
}

Evaluation.ReportData = function( params, result){
	// logic gate
	// 0 0
	// 0 1
	// 1 0
	// 1 1
	// agent_id to_date

	// form_id
	// from_date
	// to_date
	// agent_id
	// project_id

	var data;
	if (params.to_date == params.from_date){ params.to_date = 'undefined' }

	if(params.agent_id == 'undefined' && params.to_date == 'undefined'){
		var runQuery = 	"SELECT * FROM evaluations "+
										" WHERE form_id = '"+params.form_id+
										"' AND created_at LIKE '"+params.from_date+"%'";
	}
	if(params.agent_id == 'undefined' && params.to_date != 'undefined'){
		var runQuery = 	"SELECT * FROM evaluations "+
										" WHERE form_id = '"+params.form_id+
										"' AND created_at >= '"+params.from_date+
										"' AND created_at <= '"+params.to_date+" 23:59:59'";
	}
	if(params.agent_id != 'undefined' && params.to_date == 'undefined'){
		var runQuery = 	"SELECT * FROM evaluations "+
										" WHERE form_id = '"+params.form_id+
										"' AND agent_id = '"+params.agent_id+
										"' AND created_at LIKE '"+params.from_date+"%'";	
	}
	if(params.agent_id != 'undefined' && params.to_date != 'undefined'){
		var runQuery = 	"SELECT * FROM evaluations "+
										" WHERE form_id = '"+params.form_id+
										"' AND agent_id = '"+params.agent_id+
										"' AND created_at >= '"+params.from_date+
										"' AND created_at <= '"+params.to_date+" 23:59:59'";
	}

  sql.query(runQuery, function (err, res) {
		if(err)
			result(null, err);
		res.length == 0 ? result(null, res) : setValue(res);
	});

	function setValue(res) {
	  data = res;
	  var report_data = [];
		for (var i = data.length - 1; i >= 0; i--) {
			myIndex = i
			item = data[i]
		  Evaluation.CalcReport( item['id'], function(err, data2) {
				for (var j = data2.form_categories_score.length - 1; j >= 0; j--) {
			  	var colorCode;
				  Evaluation.Colors( data2.form_categories_score[j]['cat_score'], function(err, data3) { colorCode = data3 });
			  	data2.form_categories_score[j]['color_code'] = colorCode;
				}
		  	var form_colorCode;
			  Evaluation.Colors( data2.form_score, function(err, data4) { form_colorCode = data4 });
			  data2.form_colorCode = form_colorCode;
				report_data.push(data2);
				if (err)
					result(null, err);
		    returnValue(report_data);
		  });
		}
	}

	function returnValue(report) {
		// to return the results after the final item in loop
		if (report.length == data.length){
			var runQuery = 	" SELECT CONCAT( u.first_name, ' ', u.last_name ) AS gc_names, up.user_id, up.project_id "+ 
											" FROM user_projects up "+
											" INNER JOIN users u ON (u.id = up.user_id AND u.role_id = 3) "+
											" WHERE up.project_id = "+ params.project_id;
		  sql.query(runQuery, function (err, gcs) {
				if(err)
					result(null, err);
				let i = 0;
				let finalReport = [];
				let displayColumns = [];
				var form_report;
				gcs.forEach(function(gc){ //for indivial gc
					i++;
					let gcAgentReport = [];
					report.forEach(function(rep){
						if (gc.user_id == rep.gc_id) {
							gcAgentReport.push(rep); // list of agents for indivial gc
						}
					})
					var arr2 = gcAgentReport.reduce( (a,b) => { // find unique agent from list of agents for indivial gc
				    var i = a.findIndex( x => x.agent_id === b.agent_id);
				    return i === -1 ? a.push({ agent_names: b.agent_names, agent_id : b.agent_id, times : 1 }) : a[i].times++, a;
					}, []);
					form_report = arr2;
					let agentAverageScore = [];
					arr2.forEach(function(unique_agent){ // for each unique agent
						var agent_data = [];
						gcAgentReport.forEach(function(agent_report){
							if (unique_agent.agent_id == agent_report.agent_id) {
								agent_data.push(agent_report); // data for specific agent
							}
						});
						var form_categories = [];
						var form_count = 0;
						var avg_form_score = 0;
						var total_form_score = 0;
						agent_data.forEach(function(avg){
							form_count++;
							total_form_score = total_form_score+avg.form_score;
							avg.form_categories_score.forEach(function(frm){
								form_categories.push(frm);
							});
						});
						avg_form_score = Math.round(total_form_score/form_count);
						unique_agent.avg_form_score = avg_form_score;
						Evaluation.Colors( avg_form_score, function(err, color_response) { unique_agent.avg_color_code = color_response });
						var arr4 = form_categories.reduce( (a,b) => { // unique form category
					    var i = a.findIndex( x => x.form_cat_id === b.form_cat_id);
					    return i === -1 ? a.push({ color_code: b.color_code, form_cat_name: b.form_cat_name, form_cat_id : b.form_cat_id, times : 1 }) : a[i].times++, a;
						}, []);
						arr4.forEach(function(unique_cat){
							var cat_count = 0;
							var avg_cat_score = 0;
							var total_cat_score = 0;
							form_categories.forEach(function(all_cat){
								if (unique_cat.form_cat_id == all_cat.form_cat_id) {
									cat_count++;
									total_cat_score = total_cat_score+all_cat.cat_score;
								}
							});
							avg_cat_score = Math.round(total_cat_score/cat_count)
							unique_cat.avg_cat_score = avg_cat_score;
						});
						unique_agent.arr4 = arr4;
						unique_agent.agent_data = agent_data;
						displayColumns = arr4;
					});
					gc['report'] = arr2;
					finalReport.push(gc);
				})
				result(null, {'finalReport': finalReport, 'displayColumns': displayColumns });
			});
		}
	}
}

Evaluation.ResponseData = function( res_id, result){
	// SELECT er.id, er.score, er.question_id, er.evaluation_id, q.text, e.instance, e.recommended_action, e.status
	// FROM evaluation_results er
	// INNER JOIN question q ON q.id = er.question_id
	// INNER JOIN evaluations e ON e.id = er.evaluation_id
	// WHERE er.evaluation_id = 23
	var runQuery1 = " SELECT er.id, er.score, er.question_id, er.evaluation_id, q.text "+ 
									" FROM evaluation_results er "+
									" INNER JOIN question q ON q.id = er.question_id "+
									" WHERE er.evaluation_id = "+ res_id;
  sql.query(runQuery1, function (err, res1) {
		if(err){
			result(null, err);
		}else{
			var runQuery2 = " SELECT e.instance, e.recommended_action, e.status, e.comments "+
											" FROM evaluations e "+
											" WHERE e.id = "+ res_id
		  sql.query(runQuery2, function (err, res2) {
				if(err)
					result(null, err);
				result(null, {'evaluation_results': res1, 'evaluation': res2[0]});
			});
		}

	});
}

module.exports = Evaluation;
