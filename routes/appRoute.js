'use strict';
module.exports = function(router) {
  var todoList = require('../controllers/appController');
  var accountController = require('../controllers/accountController');
  var qaController = require('../controllers/qaController');
	var authController = require('../controllers/authController');
	var beerController = require('../controllers/beer');

	// Create endpoint handlers for /beers
	router.route('/beers')
	  .post(authController.isAuthenticated, beerController.postBeers)
	  .get(authController.isAuthenticated, beerController.getBeers);

	// Create endpoint handlers for /beers/:beer_id
	router.route('/beers/:beer_id')
	  .get(authController.isAuthenticated, beerController.getBeer)
	  .put(authController.isAuthenticated, beerController.putBeer)
	  .delete(authController.isAuthenticated, beerController.deleteBeer);


  // todoList Routes
  router.route('/tasks')
    .get(todoList.list_all_tasks)
    .post(todoList.create_a_task);
   
	router.route('/tasks/:taskId')
		.get(todoList.read_a_task)
		.put(todoList.update_a_task)
		.delete(todoList.delete_a_task);

	// Create endpoint handlers for /users
	router.route('/accounts/users')
	  .post(accountController.postUsers);
	  //   .get(authController.isAuthenticated, userController.getUsers);

  // accountController Routes
  router.route('/accounts/login')
    .post(accountController.userLogin);

  router.route('/accounts/users/:country_id/:is_active/')
    .get(accountController.userList)

	router.route('/accounts/user_active_toggle/')
    .post(accountController.userToggle);

  router.route('/accounts/user/:userId')
		.get(accountController.readUserDetail)
		.put(accountController.updateUser)
		.delete(accountController.deleteUser);

	router.route('/accounts/user_projects/:userId/')
		.get(accountController.getUserProjects);

  router.route('/accounts/countries')
    .get(accountController.getCountries)
    .post(accountController.createCountry);

	router.route('/accounts/search/project/:name').get(accountController.searchProject);
	router.route('/accounts/search/email/:email').get(accountController.searchEmail);
	router.route('/accounts/search/username/:username').get(accountController.searchUsername);
	router.route('/accounts/forgot_password').post(accountController.forgotPassword);
	router.route('/accounts/change_password').post(accountController.changePassword);
	router.route('/accounts/delete_user').post(accountController.deleteUser);
  router.route('/accounts/projects').post(accountController.createProject);
  router.route('/accounts/projects/:country/').get(accountController.getProjects);


  router.route('/qa/project_forms')
    .post(qaController.createProjectForms);

  router.route('/qa/project_forms/:project_id/:user_id/')
    .get(qaController.getProjectForms);

  router.route('/qa/project_users/:project_id/')
    .get(qaController.getProjectUsers);

  router.route('/qa/form/:form_id/')
    .get(qaController.getFormDetails);

  router.route('/qa/form/:form_id/')
    .get(qaController.getFormDetails);

  router.route('/qa/form_category/')
    .post(qaController.createFormCategory);

  router.route('/qa/form_category/:form_id/')
    .get(qaController.getFormCategories);

  router.route('/qa/form_category_details/:form_cat_id/')
    .get(qaController.getFormCategoryDetails);
    
  router.route('/qa/form_category_question/')
    .post(qaController.createFormCategoryQuestions);

  router.route('/qa/form_category_questions/:form_cat_id/')
    .get(qaController.getFormCategoryQuestions)
		.put(qaController.updateQuestion)
		.delete(qaController.deleteQuestion);

  router.route('/qa/response/')
    .post(qaController.createEvaluationResponse);

  router.route('/qa/response/:response_id/')
    .put(qaController.updateEvaluationResponse)

  router.route('/qa/evaluation_report/:project_id/:form_id/:from_date/:to_date/:agent_id/')
    .get(qaController.getEvaluationReport)

  router.route('/qa/evaluation_response/:res_id/')
    .get(qaController.getEvaluationResponse)

};