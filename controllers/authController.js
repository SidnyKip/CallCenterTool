// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var User = require('../models/accountModel.js');

passport.use(new BasicStrategy(
	function(username, password, res) {
		User.getUserByUsername(username, function (err, user) {
			if (err) { return res(null, err); }
			// No user found with that username
			if (user.length == 0) { 
				return res(null, false);
			}else{
				// Make sure the password is correct
				User.verifyPassword(password, user[0]['password'], function(err, isMatch) {
					if (err) { return res(err); }
					// Password did not match
					if (!isMatch) { return res(null, false); }
					// Success
					return res(null, user);
					res.json(task);
				});
			}
		});
	}
));

exports.isAuthenticated = passport.authenticate('basic', { session : false });