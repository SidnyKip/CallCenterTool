'use strict';

var User = require('../models/accountModel.js');
var Project = require('../models/projectModel.js');
var Country = require('../models/countryModel.js');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;

exports.userList = function(req, res) {
  User.getUsers(req.params, function(err, users) {
		if (err)
		  res.send(err);
		res.json(users);
  });
};

exports.postUsers = function(req, res) {
  // var new_user = new User(req.body);
  var new_user = req.body;
	//handles null error 
	if(!new_user.username || !new_user.email){
		res.status(400).send({ error:true, message: 'Please provide username/email'});
	}else{
	  User.createUser(new_user, function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
	    // res.json({ message: 'New beer drinker added to the locker room!' });
	  });
	}
};

exports.userToggle = function(req, res) {
  User.user_active_toggle(req.body, function(err, task) {
	if (err)
	  res.send(err);
	res.json(task);
  });
};

exports.readUserDetail = function(req, res) {
  User.getUserById(req.params.userId, function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
};

exports.updateUser = function(req, res) {
  User.updateById(req.params.userId, new User(req.body), function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
};

exports.deleteUser = function(req, res) {
  User.delete_user( req.body.user_id, function(err, user_id) {
		if (err)
		  res.send(err);
		res.json({ message: 'User successfully deleted' });
  });
};

exports.searchEmail = function(req, res) {
  User.search_email( req.params.email, function(err, email) {
		if (err)
		  res.send(err);
		res.json(email);
  });
};

exports.searchUsername = function(req, res) {
  User.search_username( req.params.username, function(err, username) {
		if (err)
		  res.send(err);
		res.json(username);
  });
};

exports.forgotPassword = function(req, res) {
  User.forgot_password( req.body.email, function(err, email) {
		if (err)
		  res.send(err);
		res.json(email);
  });
};

exports.changePassword = function(req, res) {
  User.change_password({
  	'old_password': req.body.old_password, 
  	'new_password': req.body.new_password, 
  	'user_id': req.body.user_id,
  }, function(err, email) {
	if (err)
	  res.send(err);
	res.json(email);
  });
};

exports.userLogin = function(req, res) {
  var user_login = new User(req.body);
  var username = req.body.username;
  var password = req.body.password;
	//handles null error 
	if(!username || !password){
		res.status(400).send({ error:true, message: 'Please provide username/email' });
	}else{
		var auth = passport.authenticate('basic', { session : false });
		passport.use(new BasicStrategy(
			function(username, password, res) {
				User.getUserByUsername(username, function (err, user) {
					if (err) { return res(null, err); }
					// No user found with that username
					if (user.length == 0) { 
						return res(null, false);
					}else{
						// Make sure the password is correct
						User.verifyPassword(password, user[0]['password'], function(err, isMatch) {
							if (err) { return res(err); }
							// Password did not match
							if (!isMatch) { return res(null, false); }
							// Success
							return res(null, user);
							res.json(task);
						});
					}
				});
			}
		));
	}
};

// Project
exports.searchProject = function(req, res) {
  Project.search_project( req.params.name, function(err, name) {
		if (err)
		  res.send(err);
		res.json(name);
  });
};

exports.createProject = function(req, res) {
  var new_project = new Project(req.body);
	if(!new_project.name || !new_project.country_id){
		res.status(400).send({ error:true, message: 'Please provide name/country' });
	}else{
	  Project.create_project(new_project, function(err, task) {
			if (err)
			  res.send(err);
			res.json(task);
	  });
	}
};

exports.getProjects = function(req, res) {
  Project.get_projects( req.params.country,  function(err, task) {
	if (err)
	  res.send(err);
	res.send(task);
  });
};

exports.getUserProjects = function(req, res) {
  Project.get_user_projects( req.params.userId,  function(err, task) {
	if (err)
	  res.send(err);
	res.send(task);
  });
};

// Country
exports.getCountries = function(req, res) {
  Country.getAllCountries(function(err, task) {
	if (err)
	  res.send(err);
	res.send(task);
  });
};

exports.createCountry = function(req, res) {
  var new_task = new Country(req.body);
	if(!new_task.task || !new_task.status){
		res.status(400).send({ error:true, message: 'Please provide task/status' });
	}else{
	  Country.createCountry(new_task, function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
	  });
	}
};

exports.read_a_country = function(req, res) {
  Country.getCountryById(req.params.countryId, function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
};

exports.update_a_country = function(req, res) {
  Country.updateById(req.params.countryId, new Country(req.body), function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
};

exports.delete_a_country = function(req, res) {
  Country.remove( req.params.countryId, function(err, task) {
		if (err)
		  res.send(err);
		res.json({ message: 'Country successfully deleted' });
  });
};
