'use strict';

var User = require('../models/accountModel.js');
var Project = require('../models/projectModel.js');
var Country = require('../models/countryModel.js');

exports.createProjectForms = function(req, res) {
  Project.create_project_forms( req.body,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
};

exports.getProjectForms = function(req, res) {
  Project.get_project_forms( req.params,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
};

exports.getProjectUsers = function(req, res){
  Project.get_project_users( req.params.project_id,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.getFormDetails = function(req, res){
  Project.get_form( req.params.form_id,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.createFormCategory = function(req, res){
  Project.create_form_category( req.body,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.getFormCategories = function(req, res){
  Project.get_form_categories( req.params.form_id,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.getFormCategoryDetails = function(req, res){
  Project.get_form_category_details( req.params.form_cat_id,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.createFormCategoryQuestions = function(req, res){
  Project.create_form_category_question( req.body,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.getFormCategoryQuestions = function(req, res){
  Project.get_form_category_questions( req.params.form_cat_id,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.updateQuestion = function(req, res) {
  Project.update_question(req.params.form_cat_id, req.body, function(err, task) {
	if (err)
	  res.send(err);
	res.json(task);
  });
};

exports.deleteQuestion = function(req, res) {
  Project.remove_question( req.params.form_cat_id, function(err, task) {
	if (err)
	  res.send(err);
	res.json({ message: 'Question successfully deleted' });
  });
};


exports.createEvaluationResponse = function(req, res){
  Project.create_evaluation_response( req.body,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.updateEvaluationResponse = function(req, res) {
  Project.update_evaluation_response(req.params.response_id, req.body, function(err, task) {
	if (err)
	  res.send(err);
	res.json(task);
  });
};

exports.getEvaluationReport = function(req, res){
  Project.get_evaluation_report( req.params,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}

exports.getEvaluationResponse = function(req, res){
  Project.get_evaluation_response( req.params.res_id,  function(err, task) {
		if (err)
		  res.send(err);
		res.json(task);
  });
}
