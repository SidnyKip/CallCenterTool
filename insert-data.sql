INSERT INTO `roles`(`role_name`) VALUES
	('agent'),
	('qc'),
	('gc'),
	('admin');

INSERT INTO `countries`(`name`) VALUES
	('Kenya'),
	('Uganda'),
	('Tanzania'),
	('Zambia');

SELECT q.id, q.text, q.description, fc.name AS form_category, f.name AS form_name  FROM question q INNER JOIN form_cat fc ON fc.id = q.form_cat_id INNER JOIN forms f ON fc.id = f.id WHERE 