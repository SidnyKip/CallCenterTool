var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var app = express();
var port = process.env.PORT || 3000;
var cors = require('cors');
var connect = require('./db.js');

console.log('API server started on: ' + port);

app.use(cors());
app.use(bodyParser.urlencoded({
 extended: false 
}));

app.use(bodyParser.json());
app.use(passport.initialize()); // Use the passport package in our application

var router = express.Router(); // Create our Express router
app.use('/api', router);
app.listen(port);
app.use(bodyParser.json());
var routes = require('./routes/appRoute'); //importing route
routes(router); //register the route